class ActionBinder {
	instanceVariables() {
		this.dataManager = null;
		this.viewModel = null;
	}

    constructor() {
        $("#replay-btn").click(() => {
            game.playCorrectAnswer();
        });

        $("#map-replay-btn").click(() => {
            if ($("#map-replay-btn").hasClass('btn-secondary')) {
                $("#map-replay-btn").removeClass('btn-secondary')
                $("#map-replay-btn").addClass('btn-primary')

                midiMapper.mapNextKeyPressToFunction(game.playCorrectAnswer);
            } else {
                $("#map-replay-btn").removeClass('btn-primary')
                $("#map-replay-btn").addClass('btn-secondary')
            }
		});
		
		$("#clear-high-score-btn").click(() => {
			this.dataManager.levelHighScore = 0;
		});
	}

	levelSelect() {
		var self = this;

		return function() {
			var levelIndex = parseInt($(this).attr('level-index'));
			var level = self.dataManager.levels[levelIndex];
			self.dataManager.currentLevelIndex = levelIndex;
		};
	}

	midiInputSelect() {
		var self = this;

		return function() {
			var midiID = $(this).attr('midi-id');
			self.dataManager.midiInputDeviceID = midiID;
		};
	}

	midiOutputSelect() {
		var self = this;

		return function() {
			var midiID = $(this).attr('midi-id');
			self.dataManager.midiOutputDeviceID = midiID;
		};
	}
}
class MidiMapper {

    constructor() {
        this.mappings = {};

        WebMidi.enable((err) => {
            WebMidi.addListener('connected', (e) => {
                this.handleMidiConnected();
            });

            WebMidi.addListener('disconnected', (e) => {
                this.handleMidiDisconnected();
            });

            if (WebMidi.inputs.length > 0) {
                this.handleMidiConnected();
            }
        });
    }

    handleMidiConnected() {
		if (this.input) {
			this.input.removeListener();
		}

		this.input = WebMidi.inputs[0];
		this.output = WebMidi.outputs[0];

		this.startListeningForEventsFromInput(WebMidi.inputs[0]);
	}

	handleMidiDisconnected() {
		this.input = null;
		this.output = null;
	}

    startListeningForEventsFromInput(input) {
		input.addListener("noteon", "all", (e) => {
            if (e.note.number in this.mappings) {
                this.mappings[e.note.number]();
            }

            if (this.onNextKeyPressMapThisFunction) {
                this.mappings[e.note.number] = this.onNextKeyPressMapThisFunction;
                this.onNextKeyPressMapThisFunction = null;
            }
		});
    }
    
    mapNextKeyPressToFunction(fn) {
        this.onNextKeyPressMapThisFunction = fn;
    }
}
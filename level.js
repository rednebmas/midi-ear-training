class Level {
	instanceVariables() {
		this.notesMidi = null;
		this.notesAsStrings = null;
		this.index = null;
		this.number = null;
	}

	constructor(notesMidi, index) {
		this.notesMidi = notesMidi;
		this.notesAsStrings = notesMidi.map(function(val) {
			var note = teoria.note.fromMIDI(val);
			if (note.accidentalValue() < 0) {
				note = teoria.note.fromString(note.enharmonics(true).toString());
			}
			return note.toString(true).toUpperCase();
		});

		this.index = index;
		this.number = index + 1;
	} 

	randomNoteAsMidi(previousAnswer) {
		var min = 0;
		var max = this.notesMidi.length - 1; 
		var randomMidiVal = previousAnswer;
		while (randomMidiVal == previousAnswer) {
			randomMidiVal = this.notesMidi[this.getRandomIntInclusive(min, max)];
		}
		return randomMidiVal;
	}

	getRandomIntInclusive(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min; // The maximum is inclusive and the minimum is inclusive 
	}
}
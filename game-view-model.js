class GameViewModel {
	constructor(actionBinder) {
		this.actionBinder = actionBinder;
	}

	set streak(value) {
		$('#streak').html('Streak: ' + value)
	}

	set totalQuestionsAnswered(value) {
		$('#total-questions').html('Total Questions Answered: ' + value)
	}

	set levelHighScore(value) {
		if (value == undefined) {
			value = 0;
		}
		if (value == 20) {
			// CHANGE THIS VVVVVV
			// WebMidi.outputs[0].playNote(["C3", "E3", "G3"], 7, { duration: 1250 });
		}
		$('#level-high-score').html('High score: ' + value);
	}

	set currentLevel(level) {
		$('#level-dropdown-btn').html(this.levelHTMLFromLevel(level));
	}

	set levels(levels) {
		$.each(levels, (i, level) => {
			$('<a/>', {
				html: this.levelHTMLFromLevel(level),
				class: 'dropdown-item',
				'level-index': level.index,
				click: this.actionBinder.levelSelect()
			}).appendTo('#level-dropdown');
		});
	}

	levelHTMLFromLevel(level) {
		return '<strong>Level ' + level.number + ':</strong> ' + level.notesAsStrings.join(', ');
	}

	set midiInputDeviceID(inputDeviceID) {
		var inputDevice = WebMidi.getInputById(inputDeviceID);
		if (!inputDevice) return;
		console.log('input device name: ' + inputDevice.name);

		$('#midi-input-dropdown-btn').html('<strong>Input:</strong> ' + inputDevice.name);
	}

	set midiOutputDeviceID(outputDeviceID) {
		var outputDevice = WebMidi.getOutputById(outputDeviceID);
		if (!outputDevice) return;
		console.log('output device name: ' + outputDevice.name);

		$('#midi-output-dropdown-btn').html('<strong>Output:</strong> ' + outputDevice.name);
	}

	midiConnected(inputs, outputs) {
		$('#midi-connected').css('visibility', 'visible');

		$('#midi-input-dropdown').empty();
		$.each(inputs, (i, input) => {
			$('<a/>', {
				html: input.name,
				class: 'dropdown-item',
				'midi-id': input.id,
				click: this.actionBinder.midiInputSelect()
			}).appendTo('#midi-input-dropdown');
		});


		$('#midi-output-dropdown').empty();
		$.each(outputs, (i, output) => {
			$('<a/>', {
				html: output.name,
				class: 'dropdown-item',
				'midi-id': output.id,
				click: this.actionBinder.midiOutputSelect()
			}).appendTo('#midi-output-dropdown');
		});
	}

	midiDisconnected() {
		$('#midi-connected').css('visibility', 'hidden');
	}
}
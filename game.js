class Game {
	instanceVariables() {
		this.output = null;
		this.input = null;
		this.playNextDelay = 500;
		this.answer = null;
		this.previousAnswer = null;
		this.mostRecentAnswerPlayTime = null;
		this.skipNextNoteOff = false;
	}

	instanceVariablesThatNeedWebMidi() {
		this.actionBinder = new ActionBinder();
		this.viewModel = new GameViewModel(this.actionBinder);
		this.dataManager = new GameDataManager(this.viewModel);

		this.actionBinder.viewModel = this.viewModel;
		this.actionBinder.dataManager = this.dataManager;
		this.viewModel.dataManager = this.dataManager;
	}

	constructor() {
		this.instanceVariables();
		WebMidi.enable((err) => {
			if (err) {
				alert("Error: WebMidi could not be enabled. Please contact the administrator");
				return;
			}

			this.instanceVariablesThatNeedWebMidi();

			WebMidi.addListener('connected', (e) => {
				this.handleMidiConnected();
			});

			WebMidi.addListener('disconnected', (e) => {
				this.handleMidiDisconnected();
			});

			if (WebMidi.inputs.length > 0) {
				this.handleMidiConnected();
			}

			this.start();
		});
	}

	//
	// MIDI
	//

	handleMidiConnected() {
		this.viewModel.midiConnected(WebMidi.inputs, WebMidi.outputs);

		if (this.input) {
			this.input.removeListener();
		}

		this.input = WebMidi.getInputById(this.dataManager.midiInputDeviceID);
		this.output = WebMidi.getOutputById(this.dataManager.midiOutputDeviceID);

		if (this.input) {
			this.startListeningForEventsFromInput(this.input);
		}
	}

	startListeningForEventsFromInput(input) {
		// Listening for a 'note on' message (on all channels) 
		input.addListener("noteon", "all", (e) => {
			this.handleNoteOn(e);
		});

		// Listening to other messages works the same way 
		input.addListener("noteoff", "all", (e) => {
			this.handleNoteOff(e);
		});
	}

	handleMidiDisconnected() {
		this.viewModel.midiDisconnected();
		this.input = null;
		this.output = null;
	}

	//
	// Game logic
	// 

	handleNoteOn(msg) {
		if (document.hidden) return;
		if (msg.velocity > 0 && msg.note.number == this.answer) {
			// prevent looping
			if (new Date() - this.mostRecentAnswerPlayTime - this.playNextDelay < 50)  {
				this.skipNextNoteOff = true;
				return; 
			}

			this.dataManager.streak += 1;
			this.dataManager.totalQuestionsAnswered += 1;
			$('body').css('background-color', 'green');
		} else if (msg.note.number != this.answer) {
			this.dataManager.streak = 0;
		}
	}

	handleNoteOff(msg) {
		if (document.hidden) return;
		if (msg.note.number == this.answer) {
			if (this.skipNextNoteOff) {
				this.skipNextNoteOff = false;
				return;
			}

			$('body').css('background-color', 'black');
			this.generateCorrectAnswer();
			this.playCorrectAnswer(this.playNextDelay);
		}
	}

	start() {
		this.generateCorrectAnswer();
		setTimeout(() => {
			this.playCorrectAnswer();
		}, 500);
	}

	playCorrectAnswer(delay = 0) {
		if (!this.output) return;
		this.output.playNote(this.answer, 1, {duration: 1250, velocity: 0.55, time: '+' + delay});
		this.mostRecentAnswerPlayTime = new Date();
	}

	generateCorrectAnswer(self) {
		this.previousAnswer = this.answer;
		this.answer = this.dataManager.currentLevel.randomNoteAsMidi(this.previousAnswer);
	}
}

$(document).ready(() => {
	game = new Game();
	midiMapper = new MidiMapper();
});
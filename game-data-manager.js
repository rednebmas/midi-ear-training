class GameDataManager {
	getCookie(name, defaultVal) {
		var val = Cookies.get(name);
		if (val) return val;
		else return defaultVal;
	}

	constructor(viewModel) {
		this.viewModel = viewModel;
		this.streak = 0;

		this.totalQuestionsAnswered = parseInt(this.getCookie('totalQuestionsAnswered', '0'));
		this.levels = levels.map((level, i) => { return new Level(level, i); })
		this.levelHighScore = parseInt(this.getCookie('levelHighScore', '0'));
		this.currentLevelIndex = parseInt(this.getCookie('currentLevelIndex', '0'));
		this.midiInputDeviceID = this.getCookie('midiInputDeviceID', WebMidi.inputs.length ? WebMidi.inputs[0].id : '0');
		this.midiOutputDeviceID = this.getCookie('midiOutputDeviceID', WebMidi.inputs.length ? WebMidi.outputs[0].id : '0');
	}

	get streak() { return this._streak; }
	set streak(value) {
		this._streak = value;
		this.viewModel.streak = value;

		if (this.streak > this.levelHighScore) {
			this.levelHighScore = this.streak;
		}
	}

	get totalQuestionsAnswered() { return this._totalQuestionsAnswered; }
	set totalQuestionsAnswered(value) {
		this._totalQuestionsAnswered = value;
		this.viewModel.totalQuestionsAnswered = value;
		Cookies.set('totalQuestionsAnswered', ''+value);
	}

	get levelHighScore() { return this._levelHighScore; }
	set levelHighScore(value) {
		this._levelHighScore = value;

		this.viewModel.levelHighScore = value;
		Cookies.set('levelHighScore', ''+value);
	}

	get currentLevel() { return this._levels[this._currentLevelIndex] };
	get currentLevelIndex() { return this._currentLevelIndex };
	set currentLevelIndex(value) {
		this._currentLevelIndex = value;
		
		this.viewModel.currentLevel = this.levels[value];
		Cookies.set('currentLevelIndex', ''+value)
	}

	get levels() { return this._levels; }
	set levels(value) { 
		this._levels = value;
		this.viewModel.levels = value;
	}

	get midiInputDeviceID() { return this._midiInputDeviceID; }
	set midiInputDeviceID(value) {
		this._midiInputDeviceID = value;
		this.viewModel.midiInputDeviceID = value;

		Cookies.set('midiInputDeviceID', ''+value)
	}

	get midiOutputDeviceID() { return this._midiOutputDeviceID; }
	set midiOutputDeviceID(value) {
		this._midiOutputDeviceID = value;
		this.viewModel.midiOutputDeviceID = value;

		Cookies.set('midiOutputDeviceID', ''+value)
	}
}